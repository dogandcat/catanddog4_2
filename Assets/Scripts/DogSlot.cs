﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DogSlot : MonoBehaviour
{


    private DogInventory inventoryDog;
    public int i;

    private void Start()
    {

        inventoryDog = GameObject.FindGameObjectWithTag("Dog").GetComponent<DogInventory>();
    }

    private void Update()
    {
        if (transform.childCount <= 0)
        {
            inventoryDog.isFullDog[i] = false;
        }
    }

}
