﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{
    private DogInventory inventoryDog;
    private CatInventory inventoryCat;
    public GameObject itemButtonDog;
    public GameObject itemButtonCat;
    //public AudioSource tickSource;
    //public AudioClip clip;

    private void Start()
    {
        inventoryDog = GameObject.FindGameObjectWithTag("Dog").GetComponent<DogInventory>();
        inventoryCat = GameObject.FindGameObjectWithTag("Cat").GetComponent<CatInventory>();

        //tickSource = GetComponent<AudioSource>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Dog"))
        {
            for (int i = 0; i < inventoryDog.slotsDog.Length; i++)
            {
                if (inventoryDog.isFullDog[i] == false)
                {
                    //item can be added to inventory
                    inventoryDog.isFullDog[i] = true;
                    Instantiate(itemButtonDog, inventoryDog.slotsDog[i].transform, false);
                    Destroy(gameObject);
                    break;
                }
            }
            //if (clip) AudioSource.PlayClipAtPoint(clip, transform.position);
        }

        if (other.CompareTag("Cat"))
        {
            for (int i = 0; i < inventoryCat.slotsCat.Length; i++)
            {
                if (inventoryCat.isFullCat[i] == false)
                {
                    //item can be added to inventory
                    inventoryCat.isFullCat[i] = true;
                    Instantiate(itemButtonCat, inventoryCat.slotsCat[i].transform, false);
                    Destroy(gameObject);
                    break;
                }
            }
            //if (clip) AudioSource.PlayClipAtPoint(clip, transform.position);
        }
    }
}
