﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatSlot : MonoBehaviour
{


    private CatInventory inventoryCat;
    public int i;

    private void Start()
    {

        inventoryCat = GameObject.FindGameObjectWithTag("Cat").GetComponent<CatInventory>();
    }

    private void Update()
    {
        if (transform.childCount <= 0)
        {
            inventoryCat.isFullCat[i] = false;
        }
    }

}
