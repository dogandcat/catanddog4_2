﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DogPickup : MonoBehaviour
{
    private DogInventory inventoryDog;
    public GameObject itemButtonDog;
    //public AudioSource tickSource;
    //public AudioClip clip;

    private void Start()
    {
        inventoryDog = GameObject.FindGameObjectWithTag("Dog").GetComponent<DogInventory>();
        //tickSource = GetComponent<AudioSource>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Dog"))
        {
            for (int i = 0; i < inventoryDog.slotsDog.Length; i++)
            {
                if (inventoryDog.isFullDog[i] == false)
                {
                    //item can be added to inventory
                    inventoryDog.isFullDog[i] = true;
                    Instantiate(itemButtonDog, inventoryDog.slotsDog[i].transform, false);
                    Destroy(gameObject);
                    break;
                }
            }
            //if (clip) AudioSource.PlayClipAtPoint(clip, transform.position);
        }
    }
}
