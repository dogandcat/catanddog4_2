﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatPickup : MonoBehaviour
{
    private CatInventory inventoryCat;
    public GameObject itemButtonCat;
    //public AudioSource tickSource;
    //public AudioClip clip;

    private void Start()
    {
        inventoryCat = GameObject.FindGameObjectWithTag("Cat").GetComponent<CatInventory>();
        //tickSource = GetComponent<AudioSource>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Cat"))
        {
            for (int i = 0; i < inventoryCat.slotsCat.Length; i++)
            {
                if (inventoryCat.isFullCat[i] == false)
                {
                    //item can be added to inventory
                    inventoryCat.isFullCat[i] = true;
                    Instantiate(itemButtonCat, inventoryCat.slotsCat[i].transform, false);
                    Destroy(gameObject);
                    break;
                }
            }
            //if (clip) AudioSource.PlayClipAtPoint(clip, transform.position);
        }
    }
}
