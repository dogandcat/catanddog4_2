﻿using UnityEngine;

public class FloatingObj : MonoBehaviour
{
    public Vector3 offset;
    public float frequency;
    public bool playAwake=true;

    private Vector3 originPosition;
    private float tick;
    private float amplitude;
    private bool animate;

    void Awake()
    {

        if (Mathf.Approximately(frequency, 0))
            frequency = 1f;

        originPosition = transform.localPosition;
        tick = Random.Range(0f, 2f * Mathf.PI);

        amplitude = 2 * Mathf.PI / frequency;
        animate = playAwake;
    }

    public void Play()
    {
        transform.localPosition = originPosition;
        animate = true;
    }

    public void Stop()
    {
        transform.localPosition = originPosition;
        animate = false;
    }

    void FixedUpdate()
    {
        if (animate)
        {

            tick = tick + Time.fixedDeltaTime * amplitude;

            var amp = new Vector3(Mathf.Cos(tick) * offset.x, Mathf.Sin(tick) * offset.y, 0);

            transform.localPosition = originPosition + amp;
        }
    }
}
