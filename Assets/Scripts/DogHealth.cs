﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DogHealth : MonoBehaviour
{
    public int dogHealth;
    public Image imageDog;

    private AudioSource audioSource;

    private void Awake()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "CatBullet" || collision.tag == "AirplaneBullet")
        {
            audioSource.Play();
            dogHealth -= 1;
            imageDog.fillAmount -= 0.2f;
        }
        if (dogHealth == 0)
        {
            Debug.Log("Cat won this match!");
            SceneManager.LoadScene(2);
        }

    }
}
