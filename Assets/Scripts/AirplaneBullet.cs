using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirplaneBullet : MonoBehaviour
{

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (GetComponent<Collider>().tag == "Ground" || GetComponent<Collider>().tag == "Player" || GetComponent<Collider>().tag == "House")
        {
            Destroy(gameObject);
        }
    }
}
