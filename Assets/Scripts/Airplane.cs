﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Airplane : MonoBehaviour
{
    public Transform point1;
    public Transform point2;
    //public Transform dog;
    //public Transform cat;
    //public GameObject bullet;
    //private AudioSource audiosource;//fire
    private float airplaneSpeed;
    private bool turn;
    //private bool isDog;
    //private bool isCat;
    //private bool isFire;
    //private int currentChildrenCount;


    private void Awake()
    {
        //audiosource = gameObject.GetComponent<AudioSource>();
    }

    private void Start()
    {
        airplaneSpeed = 4.0f;
        turn = true;
        //isFire = false;
        //isDog = false;
        //isCat = false;

    }

    private void FixedUpdate()
    {
        //x = x + 2.0f*Time.deltaTime;
        //airplaneSpeed =Mathf.Abs(Mathf.Cos(x) * 10);

        //move
        //if (isFire==false)
        //{
            if (turn == true)//left->right
            {
                transform.position = new Vector3(transform.position.x + airplaneSpeed * Time.deltaTime, 3.0f, 0.0f);
                if (transform.position.x >= point2.position.x)
                {
                    turn = false;
                    transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
                }
            }
            else//right->left
            {
                transform.position = new Vector3(transform.position.x - airplaneSpeed * Time.deltaTime, 3.0f, 0.0f);
                if (transform.position.x <= point1.position.x)
                {
                    turn = true;
                    transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                }
            }
        //}
        //else
        //{
        //    //move to cat or dog and fire
        //    currentChildrenCount = transform.childCount;
        //    if (isDog==true)
        //    {
        //        if(transform.position.x<dog.position.x-0.5f)
        //        {
        //            transform.position = new Vector3(transform.position.x + airplaneSpeed * Time.deltaTime, 3.0f, 0.0f);
        //            transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
        //            if (transform.position.x <= dog.position.x + 1.0f && transform.position.x >= dog.position.x - 1.0f)
        //            {

        //                AFire();
        //            }
        //        }
        //        else if(transform.position.x > dog.position.x+0.5f)
        //        {
        //            transform.position = new Vector3(transform.position.x - airplaneSpeed * Time.deltaTime, 3.0f, 0.0f);
        //            transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
        //            if (transform.position.x <= dog.position.x + 1.0f && transform.position.x >= dog.position.x - 1.0f)
        //            {

        //                AFire();
        //            }
        //        }
        //    }

        //    if (isCat == true)
        //    {
        //        if (transform.position.x < cat.position.x-0.5f)
        //        {
        //            transform.position = new Vector3(transform.position.x + airplaneSpeed * Time.deltaTime, 3.0f, 0.0f);
        //            transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
        //            if(transform.position.x <= cat.position.x + 1.0f&&transform.position.x>= cat.position.x - 1.0f)
        //            {
        //                //if (IsInvoking("AFire")==false)
        //                //{
        //                //    InvokeRepeating("AFire", 0.5f, 3);
        //                //}      
        //                AFire();
        //            }
                    
        //        }
        //        else if (transform.position.x > cat.position.x+0.5f)
        //        {
        //            transform.position = new Vector3(transform.position.x - airplaneSpeed * Time.deltaTime, 3.0f, 0.0f);
        //            transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
        //            if (transform.position.x <= cat.position.x + 1.0f && transform.position.x >= cat.position.x -1.0f)
        //            {
        //                //if (IsInvoking("AFire") == false)
        //                //{
        //                //    InvokeRepeating("AFire", 0.5f, 3);
        //                //}
        //                AFire();
        //            }
        //        }
        //    }

        //    //if(currentChildrenCount==1)
        //    //{
        //    //   // CancelInvoke("AFire");
        //    //    isCat = false;
        //    //    isDog = false;
        //    //    isFire = false;

        //    //    if (transform.localScale.x==-1.0f)
        //    //    {
        //    //        turn = false;
        //    //    }else
        //    //    {
        //    //        turn = true;
        //    //    }
        //    //}
        //}
     
    }

    //public void AFire()
    //{
    //    Instantiate(bullet, transform.position, transform.rotation, transform);
    //    if (currentChildrenCount == 1)
    //    {
    //        // CancelInvoke("AFire");
    //        isCat = false;
    //        isDog = false;
    //        isFire = false;

    //        if (transform.localScale.x == -1.0f)
    //        {
    //            turn = false;
    //        }
    //        else
    //        {
    //            turn = true;
    //        }
    //    }
    //}

    //private void OnTriggerEnter2D(Collider2D collision)
    //{

    //    if(collision.tag=="DogBullet")
    //    {
    //        isFire = true;
    //        isDog = true;
    //    }
    //    else if(collision.tag == "CatBullet")
    //    {
    //        isFire = true;
    //        isCat = true;
    //    }
    //}
}
