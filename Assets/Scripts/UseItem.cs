﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UseItem : MonoBehaviour
{
    private Transform dog;
    private Transform cat;
    private TurnControl turncon;

    public int add = 1;

    private void Start()
    {
        dog = GameObject.FindGameObjectWithTag("Dog").transform;
        cat = GameObject.FindGameObjectWithTag("Cat").transform;
        turncon = GameObject.FindGameObjectWithTag("turn").GetComponent<TurnControl>();
    }

    public void UseInvincibility()
    {

        if (turncon.isDogAction)
        {
            Debug.Log("Dog gets health");
            DogHealth statsDog = dog.GetComponent<DogHealth>();
            if (statsDog.dogHealth < 5)
            {
                statsDog.dogHealth += add;
                statsDog.imageDog.fillAmount += 0.2f;
            }
        }
        else
        {
            if(gameObject == cat.GetComponent<CatInventory>().slotsCat[2])
            {
              
                Debug.Log("Cat gets health");
                CatHealth statsCat = cat.GetComponent<CatHealth>();
                if(statsCat.catHealth<5)
                {
                    statsCat.catHealth += add;
                    statsCat.imageCat.fillAmount += 0.2f;
                }
            }

        }

        Destroy(gameObject);
    }
    
    public void UseDoubleThrow()
    {
        //if (.tag == ("Cat"))
        //{
        //    CatHealth statsCat = cat.GetComponent<CatHealth>();
        //    statsCat.catHealth += add;
        //    statsCat.imageCat.fillAmount += 0.2f;
        //}

        Destroy(gameObject);
    }

    public void UseBrick()
    {
        Destroy(gameObject);
    }
}
